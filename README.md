Project is still work in progress.

This repository is my implementation of this paper https://arxiv.org/pdf/1611.01603.pdf for question answering task. The SQuAD is used as a dataset for training and testings.

## Model
Below is a graphic showing the architecture of the model. Picture comes from paper linked above.

![alt text](src/model.png "Model")

## Instalation

    pip install -e .

