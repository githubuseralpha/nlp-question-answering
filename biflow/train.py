import argparse
import random

import torch
from torchtext.datasets import SQuAD1
from torch.utils.data import DataLoader
from torchtext.data.utils import get_tokenizer
from torchtext.vocab import GloVe

from biflow.model.biflow_net import BidirectionalAttentionNet
from biflow.dataset.data_pipeline import DataPipeline
from biflow.model.loss import negative_log_loss
from biflow.model.metrics import accuracy_score
from biflow.config.config_parser import ConfigParser
from biflow.utils import plot_results


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, default="config/config.yaml")
    return parser.parse_args()


def _preprocess_data(contexts, questions, answers, pipeline, device="cpu"):
    contexts_transformed = pipeline.proccess_texts(contexts)
    questions_transformed = pipeline.proccess_texts(questions)

    answer_idxs = [
        pipeline.extract_answer(context, answer)
        for context, answer in zip(contexts, answers)
    ]
    answer_idxs = torch.tensor(answer_idxs)
    answer_idxs = answer_idxs.to(device)

    return contexts_transformed, questions_transformed, answer_idxs


def validate(model, dataloader, text_pipeline, device="cpu", data_frac=1.0):
    model.eval()
    losses = []
    accuracies = []
    for contexts, questions, answers, _ in dataloader:
        skip = random.random() > data_frac
        if skip:
            continue
        
        contexts_transformed, questions_transformed, answer_idxs = _preprocess_data(
            contexts, questions, answers[0], text_pipeline, device=device
        )

        with torch.no_grad():
            prediction = model(contexts_transformed, questions_transformed)

        prediction = prediction.unsqueeze(1)
        loss = negative_log_loss(prediction, answer_idxs)
        accuracy = accuracy_score(prediction, answer_idxs)
        
        losses.append(loss.item())
        accuracies.append(accuracy.item())

    return torch.mean(torch.tensor(losses)), torch.mean(torch.tensor(accuracies))


def train_epoch(
    model, optimizer, dataloader, text_pipeline, device="cpu", data_frac=1.0
):
    losses = []
    model.train()
    for contexts, questions, answers, _ in dataloader:
        skip = random.random() > data_frac
        if skip:
            continue
        contexts_transformed, questions_transformed, answer_idxs = _preprocess_data(
            contexts, questions, answers[0], text_pipeline, device=device
        )

        optimizer.zero_grad()
        prediction = model(contexts_transformed, questions_transformed)

        loss = negative_log_loss(prediction, answer_idxs)

        loss.backward()
        optimizer.step()
        
        losses.append(loss.item())
    return torch.mean(torch.tensor(losses))


def train(config, data_frac=1.0):
    device = config["device"]
    train_data, val_data = SQuAD1()

    train_dataloader = DataLoader(
        train_data, batch_size=config["batch_size"], shuffle=True
    )
    test_dataloader = DataLoader(
        val_data, batch_size=1, shuffle=True
    )

    text_pipeline = DataPipeline(
        get_tokenizer("basic_english"), GloVe(name="6B", dim=100), device=device
    )

    model = BidirectionalAttentionNet(len(text_pipeline.CHARS) + 1).to(device)
    optimizer = config["optimizer_class"](model.parameters(), **config["optimizer"])
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer,
        patience=config["lr_scheduler"]["patience"],
        factor=config["lr_scheduler"]["factor"]
    ) if "lr_scheduler" in config else None
    losses, accuracies = [], []

    for epoch in range(config["epochs"]):
        train_loss = train_epoch(
            model,
            optimizer,
            train_dataloader,
            text_pipeline,
            device=device,
            data_frac=data_frac,
        )
        val_loss, val_accuracy = validate(
            model, test_dataloader, text_pipeline, device=device, data_frac=data_frac
        )
        if scheduler:
            scheduler.step(val_loss)

        print(f"Epoch: {epoch + 1} of {config['epochs']}")
        print(f"Training loss: {train_loss}")
        print(f"Validation Loss: {val_loss}, Accuracy: {val_accuracy}")

        losses.append(val_loss)
        accuracies.append(val_accuracy)

    plot_results(losses, accuracies)


if __name__ == "__main__":
    args = _parse_args()
    config = ConfigParser(args.config)
    train(config)
   