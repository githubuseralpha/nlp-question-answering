import torch


def negative_log_loss(prediction, answer_idxs):
    return -torch.mean(
        torch.sum(
            torch.log(
                torch.gather(prediction.permute(1, 2, 0), 1, answer_idxs.unsqueeze(0))
            ),
            dim=1,
        )
    )
