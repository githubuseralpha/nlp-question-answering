import torch.nn as nn


class HighwayNet(nn.Module):
    def __init__(self, in_size=200) -> None:
        super(HighwayNet, self).__init__()
        self.linear = nn.Linear(in_size, in_size)
        self.transform_gate = nn.Linear(in_size, in_size)

        self.h_activation = nn.ReLU()
        self.t_activation = nn.Sigmoid()

    def forward(self, x):
        h = self.h_activation(self.linear(x))
        t = self.t_activation(self.transform_gate(x))
        c = 1 - t
        return h * t + x * c
