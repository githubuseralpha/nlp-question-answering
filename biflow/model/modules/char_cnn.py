import torch
import torch.nn as nn


class CharCNN(nn.Module):
    def __init__(self, in_channels, out_channels=100) -> None:
        super(CharCNN, self).__init__()
        self.in_channels = in_channels
        self.conv = nn.Conv1d(in_channels, out_channels, kernel_size=5, padding=2)
        self.activation = nn.ReLU()

    def forward(self, sentence):
        new_sentence = []
        for word in sentence:
            word = nn.functional.one_hot(word, num_classes=self.in_channels)
            word = word.float()
            word = word.unsqueeze(0).permute(0, 2, 1)
            word = self.activation(self.conv(word))
            word = word.squeeze(0)
            word = nn.MaxPool1d(word.size(1))(word)
            word = word.squeeze(1)
            new_sentence.append(word)
        new_sentence = torch.stack(new_sentence)
        return new_sentence
