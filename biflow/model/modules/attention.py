import torch
import torch.nn as nn


class Attention(nn.Module):
    def __init__(self, dim=200) -> None:
        super(Attention, self).__init__()
        self.sim_weights = nn.Linear(3 * dim, 1)
        self.c2q_softmax = nn.Softmax(dim=2)
        self.q2c_softmax = nn.Softmax(dim=1)

    def forward(self, context, query):
        t, j = context.shape[1], query.shape[1]
        context_repeated = context.unsqueeze(2).repeat(1, 1, j, 1)
        query_repeated = query.unsqueeze(1).repeat(1, t, 1, 1)
        concat = torch.cat(
            [
                context_repeated,
                query_repeated,
                context_repeated * query_repeated,
            ],
            dim=3,
        )
        S = self.sim_weights(concat).squeeze(3)
        a = self.c2q_softmax(S)
        context2query = torch.bmm(a, query)

        b = self.q2c_softmax(torch.max(S, dim=2)[0])
        query2context = torch.bmm(b.unsqueeze(1), context).repeat(1, t, 1)
        G = torch.cat(
            [context, context2query, context * context2query, context * query2context],
            dim=2,
        )
        return G
