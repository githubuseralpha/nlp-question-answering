import torch
import torch.nn as nn

from biflow.model.modules.char_cnn import CharCNN
from biflow.model.modules.highway_net import HighwayNet
from biflow.model.modules.attention import Attention


class BidirectionalAttentionNet(nn.Module):
    def __init__(self, num_chars, dim=100, highway_layers=2) -> None:
        super(BidirectionalAttentionNet, self).__init__()

        self.char_cnn = CharCNN(num_chars)
        self.attention = Attention(dim * 2)
        self.highway_net = nn.ModuleList(
            [HighwayNet(dim * 2) for _ in range(highway_layers)]
        )

        self.resize_layer = nn.Linear(dim * 2, dim)
        self.begin_weights = nn.Linear(dim * 10, 1)
        self.end_weights = nn.Linear(dim * 10, 1)
        self.softmax = nn.Softmax(dim=1)

        self.contextual_lstm = nn.LSTM(
            dim, dim, num_layers=3, bidirectional=True, batch_first=True
        )
        self.modeling_lstm = nn.LSTM(
            dim * 8, dim, num_layers=2, bidirectional=True, batch_first=True
        )
        self.end_lstm = nn.LSTM(
            dim * 2, dim, num_layers=1, bidirectional=True, batch_first=True
        )

    def _forward(self, x):
        x_embs, x_encs = [], []
        max_len = -1
        for sample in x:
            x_enc, x_emb = sample
            x_enc = self.char_cnn(x_enc)
            max_len = max(max_len, x_enc.shape[0])
            x_embs.append(x_emb)
            x_encs.append(x_enc)

        x_embs = torch.stack(
            [
                nn.functional.pad(x_emb, (0, 0, 0, max_len - x_emb.shape[0]))
                for x_emb in x_embs
            ]
        )
        x_encs = torch.stack(
            [
                nn.functional.pad(x_enc, (0, 0, 0, max_len - x_enc.shape[0]))
                for x_enc in x_encs
            ]
        )
        x_concated = torch.cat((x_embs, x_encs), dim=2)
        for highway_net in self.highway_net:
            x_concated = highway_net(x_concated)
        x_resized = self.resize_layer(x_concated)
        res = self.contextual_lstm(x_resized)[0]
        return res

    def forward(self, context, query):
        context = self._forward(context)
        query = self._forward(query)

        G = self.attention(context, query)
        M = self.modeling_lstm(G)[0]

        begin_concat = torch.cat((G, M), dim=2)
        end_concat = torch.cat((G, self.end_lstm(M)[0]), dim=2)

        begin = self.softmax(self.begin_weights(begin_concat)).squeeze()
        end = self.softmax(self.end_weights(end_concat)).squeeze()

        return torch.stack([begin, end])
