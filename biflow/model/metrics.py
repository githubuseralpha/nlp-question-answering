import torch


def accuracy_score(prediction, answer_idxs):
    return torch.mean(
        torch.eq(torch.argmax(prediction, dim=2), answer_idxs.unsqueeze(2)).float()
    )
