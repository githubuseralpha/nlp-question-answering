import matplotlib.pyplot as plt


def plot_results(losses, accuracies):
    fig, ax = plt.subplots(1, 2, figsize=(12, 6))
    ax[0].plot(losses)
    ax[0].set_title("Loss")
    ax[1].plot(accuracies)
    ax[1].set_title("Accuracy")
    plt.show()
