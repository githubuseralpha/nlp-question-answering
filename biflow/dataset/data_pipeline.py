import torch


class DataPipeline:
    CHARS = "abcdefghijklmnopqrstuvwxyz0123456789,;.!?:'\"/\\|_@#$%^&*~`+-=<>()[]"

    def __init__(self, tokenizer, global_vectors, device="cpu"):
        self.tokenizer = tokenizer
        self.global_vectors = global_vectors
        self.device = device

    def _encode_chars(self, tokenized_text):
        encocded_text = [
            torch.tensor(
                [
                    self.CHARS.index(char) + 1 if char in self.CHARS else 0
                    for char in token
                ],
                device=self.device,
            )
            for token in tokenized_text
        ]
        return encocded_text

    def _embed_words(self, tokenized_text):
        vecs = self.global_vectors.get_vecs_by_tokens(
            tokenized_text, lower_case_backup=True
        )
        return vecs.to(self.device)

    def proccess_text(self, text):
        tokenized_text = self.tokenizer(text)
        encoded_text = self._encode_chars(tokenized_text)
        embedded_text = self._embed_words(tokenized_text)
        return encoded_text, embedded_text

    def proccess_texts(self, texts):
        return [self.proccess_text(text) for text in texts]

    def extract_answer(self, context, answer):
        context, answer = self.tokenizer(context), self.tokenizer(answer)
        for idx in range(len(context) - len(answer) + 1):
            if context[idx : idx + len(answer)] == answer:
                return [idx, idx + len(answer) - 1]
        return [0, 0]
