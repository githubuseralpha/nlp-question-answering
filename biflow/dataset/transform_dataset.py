import argparse
import os
import pathlib

from torchtext.datasets import SQuAD1
from torchtext.data.utils import get_tokenizer
from torchtext.vocab import GloVe
from tqdm import tqdm
import pandas as pd

from biflow.dataset.data_pipeline import DataPipeline


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--out-dir",
        type=pathlib.Path,
        default=pathlib.Path("data"),
        help="Path to output directory",
    )
    return parser.parse_args()


def _transform_dataset(data, pipeline):
    new_dataset = pd.DataFrame(columns=["context", "question", "answer"])

    for context, query, answer, _ in tqdm(data):
        transformed_context = pipeline.proccess_text(context)
        transformed_query = pipeline.proccess_text(query)
        answer_idxs = pipeline.extract_answer(context, answer[0])

        new_dataset.loc[len(new_dataset)] = [
            transformed_context,
            transformed_query,
            answer_idxs,
        ]

    return new_dataset


def create_dataset(out_dir):
    out_dir = pathlib.Path(args.out_dir)
    os.makedirs(out_dir, exist_ok=True)

    train, test = SQuAD1()
    text_pipeline = DataPipeline(
        get_tokenizer("basic_english"), GloVe(name="6B", dim=100)
    )

    train_dataset = _transform_dataset(train, text_pipeline)
    test_dataset = _transform_dataset(test, text_pipeline)

    train_path = out_dir / "train.csv"
    test_path = out_dir / "test.csv"

    train_dataset.to_csv(train_path, index=False)
    test_dataset.to_csv(test_path, index=False)


if __name__ == "__main__":
    args = parse_args()
    create_dataset(args.out_dir)
