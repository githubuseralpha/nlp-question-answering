import pathlib

import yaml
import torch


class ConfigParser:
    OPTIMIZERS = {
        "adam": torch.optim.Adam,
        "sgd": torch.optim.SGD,
        "adagrad": torch.optim.Adagrad,
        "rmsprop": torch.optim.RMSprop,
        "adadelta": torch.optim.Adadelta,
    }

    def __init__(self, config_path):
        self.config_path = config_path
        self.config = self._parse_config()

    def _parse_config(self):
        config_path = pathlib.Path(self.config_path)
        with open(config_path, "r") as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
        optimizer = config["optimizer"].pop("name")
        config["optimizer_class"] = self.OPTIMIZERS[optimizer]
        return config

    def __getitem__(self, key):
        return self.config[key]
