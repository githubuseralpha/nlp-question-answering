import setuptools

setuptools.setup(
    name="biflow",
    version="1.0.0",
    author="Patryk Świat",
    description="Hydrophones package",
    packages=setuptools.find_packages(where="."),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    py_modules=["biflow"],
    package_dir={'': '.'},

    install_requires=[
        "torch",
        "torchtext",
        "pandas",
        "numpy",
        "portalocker",
        "matplotlib",
        "tqdm",
        "pyyaml",
    ]
)
